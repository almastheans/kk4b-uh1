package com.marae.okok;

import androidx.appcompat.app.AppCompatActivity;

import android.app.admin.SystemUpdateInfo;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private EditText et_panjang, et_lebar, et_tinggi;
    private double p, l , t;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       et_panjang = findViewById(R.id.etPanjang);
        et_lebar = findViewById(R.id.etLebar);
        et_tinggi = findViewById(R.id.etTinggi);


    }


    public void hitung_keliling(View view) {
        List<String> variable = new ArrayList<>();
        data(variable);
        Double keliling = 4 * (p+l+t);

        Intent intent = new Intent(this,result.class);
        intent.putExtra("data", String.valueOf(keliling));
        intent.putExtra("result", "Keliling");

        startActivity(intent);
    
    }

    private void data(List<String> variable) {
        String panjang = et_panjang.getText().toString();
        String lebar = et_lebar.getText().toString();
        String tinggi = et_tinggi.getText().toString();
        p = Double.parseDouble(panjang);
        l = Double.parseDouble(lebar);
        t = Double.parseDouble(tinggi);
    }


    public void hitung_luas(View view) {
        List<String> variable = new ArrayList<>();
        data(variable);
        Double luas = 2*(p*l+p*t+l*t);

        Intent intent = new Intent(this, result.class);
        intent.putExtra("data",String.valueOf(luas));
        intent.putExtra("result", "Luas");

        startActivity(intent);
    }

    public void hitung_volume(View view) {
        List<String> variable = new ArrayList<>();
        data(variable);
        Double volume =  p * l * t;

        Intent intent = new Intent(this, result.class);
        intent.putExtra("data", String.valueOf(volume));
        intent.putExtra("result", "volume");

        startActivity(intent);
    }
}
